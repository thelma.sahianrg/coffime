﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CofFIME.Models.Data;
using CofFIME.Models.Core;

namespace CofFIME.Models.Service
{
    public class FacultadesService
    {
        private CafeteriasEntities cafeteriasdb = new CafeteriasEntities();

        public List<ModFacultades> ObtenerFacultades()
        {
            try
            {
                return cafeteriasdb.Facultades.Select(row => new ModFacultades { Nom_facu = row.Nom_facu, Id_facu = row.Id_facu}).OrderBy(row => row.Nom_facu).ToList();
            }
            catch(Exception e)
            {
                return null;
            }
        }

        public ModFacultades FindFacultadByNombreFacultad(string nombreFacultad)
        {
            try
            {
                return cafeteriasdb.Facultades.Where(row => row.Nom_facu == nombreFacultad).Select(x => new ModFacultades { Id_facu = x.Id_facu}).FirstOrDefault();
            }
            catch (Exception)
            {
                return new ModFacultades();
            }
        }
    }
}