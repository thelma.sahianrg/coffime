﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CofFIME.Models.Data;
using CofFIME.Models.Core;

namespace CofFIME.Models.Service
{
    public class PlatillosService : Controller
    {
        private CafeteriasEntities cafeteriasdb = new CafeteriasEntities();

        public List<ModPlatillos> ObtenerPlatillos(int idF)
        {
            List<ModPlatillos> listaPlat = new List<ModPlatillos>();
            List<Platillos> consulta = new List<Platillos>();
            try
            {

                consulta = cafeteriasdb.Platillos.Where(row => row.IdFacu_plat == idF).ToList();

                if (consulta != null)
                {
                    foreach (Platillos pt in consulta)
                    {
                        ModPlatillos mdplat = new ModPlatillos
                        {
                            Nombre = pt.Nom_plat,
                            Precio = pt.Pre_plat,
                            Descripcion = pt.Desc_plat,
                            Facultad = idF,
                            IdPlatillo = pt.Id_plat
                        };

                        listaPlat.Add(mdplat);
                    }
                }

                return listaPlat;
            }
            catch
            {
                return null;
            }
        }

        public List<ModPlatillos> ObtenerPlatillosDest(int idF)
        {
            try
            {
                List<ModPlatillos> listaPlat = new List<ModPlatillos>();
                List<Platillos> consulta = new List<Platillos>();

                consulta = cafeteriasdb.Platillos.Where(row => row.IdFacu_plat == idF).Take(4).ToList();

                if (consulta != null)
                {
                    foreach (Platillos pt in consulta)
                    {
                        ModPlatillos mdplat = new ModPlatillos
                        {
                            Nombre = pt.Nom_plat,
                            Precio = pt.Pre_plat,
                            Descripcion = pt.Desc_plat,
                            Facultad = idF,
                            IdPlatillo = pt.Id_plat
                        };

                        listaPlat.Add(mdplat);
                    }
                }

                return listaPlat;
            }
            catch(Exception e)
            {
                return listaPlat;
            }
        }

    }
}