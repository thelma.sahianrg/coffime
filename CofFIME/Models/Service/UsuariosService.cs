﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CofFIME.Models.Data;
using CofFIME.Models.Core;

namespace CofFIME.Models.Service
{
    public class UsuariosService : Controller
    {
        private CafeteriasEntities _cafeteriasdb = new CafeteriasEntities();
        public UsuariosService()
        {
        }
        public string CreateUser(Usuarios modeloUsuarios)
        {
            try
            {
                _cafeteriasdb.Usuarios.Add(modeloUsuarios); 
                _cafeteriasdb.SaveChanges();

                return "success";
            }
            catch(Exception e)
            {
                return "error";
            }
        }
        public string FindEmail(string email)
        {
            try
            {
                Usuarios objUsuarios = _cafeteriasdb.Usuarios.Where(x => x.Email_usu == email).FirstOrDefault();
                if(objUsuarios != null)
                {
                    return objUsuarios.Email_usu;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                throw new System.ArgumentException("Algo salió mal");
            }
        }

        public Usuarios FindUser(string email, string password)
        {
            try
            {
                Usuarios objUsuarios = _cafeteriasdb.Usuarios.Where(x => x.Email_usu == email && x.Contr_usu == password).FirstOrDefault();
                if (objUsuarios != null)
                {
                    return objUsuarios;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                throw new System.ArgumentException("Algo salió mal");
            }
        }
    }
}