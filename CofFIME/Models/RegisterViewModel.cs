﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace CofFIME.Models
{
    public class RegisterViewModel
    {
        [Required]
        [EmailAddress]
        [Remote(controller: "Register", action: "IsEmailInUse")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "First Name"), StringLength(30)]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "CurrentFacultad"), StringLength(30)]
        public string CurrentFacultad { get; set; }
    }
}