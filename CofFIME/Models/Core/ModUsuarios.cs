﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CofFIME.Models.Core
{
    [Table("Usuarios")]
    public class ModUsuarios 
    {
        [Column("Id_usu"), Required]
        public long? IdUsuario;

        [Column("Contr_usu"), Required, StringLength(30)]
        public string ContrasenaUsuario;

        [Column("Nom_usu"), Required, StringLength(30)]
        public string NombreUsuario;

        [Column("Email_usu"), Required, StringLength(50)]
        public string EmailUsuario;

        [Column("IdFacu_usu"), Required, StringLength(50)]
        public long? IdFacultadUsuario;
    }
}