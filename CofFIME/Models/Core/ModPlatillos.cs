﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CofFIME.Models.Core
{
    public class ModPlatillos
    {
        public string Nombre { get; set; }
        public double Precio { get; set; }
        public string Descripcion { get; set; }
        public int Facultad { get; set; }
        public int IdPlatillo { get; set; }
    }
}