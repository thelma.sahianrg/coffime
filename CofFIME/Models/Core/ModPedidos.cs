﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CofFIME.Models.Core
{
    public class ModPedidos
    {
        public int IdFac { get; set; }
        public int IdUsr { get; set; }
        public int IdPlat { get; set; }
    }

    public class DatosCarrito
    {
        public string NombrePlat { get; set; }
        public double PrecioPlat { get; set; }
        public int IdPlat { get; set; }
        public int IdPed { get; set; }
    }
}