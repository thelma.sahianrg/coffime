﻿using CofFIME.Models.Data;
using CofFIME.Models.Service;
using System.Linq;
using System.Web.Mvc;

namespace CofFIME.Controllers
{
    public class LoginController : Controller
    {
        private UsuariosService _usuariosService = new UsuariosService();
        // GET: Login
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(string Email, string Password)
        {
            if (ModelState.IsValid)
            {
                Usuarios objUsuarios = _usuariosService.FindUser(Email, Password);
                if (objUsuarios != null)
                {
                    //add session
                    Session["FirstName"] = objUsuarios.Nom_usu;
                    Session["Email"] = objUsuarios.Email_usu;
                    return RedirectToAction("index", "home");
                }
                else
                {
                    ViewBag.error = "El email " + Email + " no está registrado";
                }
            }
            return View();
        }


        //Logout
        public ActionResult Logout()
        {
            Session.Clear();//remove session
            return RedirectToAction("Login");
        }
    }
}