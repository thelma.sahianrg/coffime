﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CofFIME.Models.Core;
using CofFIME.Models.Service;
using CofFIME.Models.Data;


namespace CofFIME.Controllers
{
    public class PedidosController : Controller
    {
        private FacultadesService facuService = new FacultadesService();
        private PlatillosService platService = new PlatillosService();
        private CafeteriasEntities db = new CafeteriasEntities();
        // GET: Pedidos
        public ActionResult Index()
        {
            List<ModFacultades> listafacultades = facuService.ObtenerFacultades();
            ViewBag.Facultades = listafacultades;

            return View();
        }

        public JsonResult GetPlatillos (int id)
        {
            List<ModPlatillos> listaplatillos = platService.ObtenerPlatillos(id);

            return Json(listaplatillos, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetCarrito()
        {
            List<DatosCarrito> dcart = new List<DatosCarrito>();

            var query = from ped in db.Pedidos
                        join plat in db.Platillos on ped.IdPlat_ped equals plat.Id_plat
                        select new DatosCarrito
                        {
                            NombrePlat = plat.Nom_plat,
                            PrecioPlat = plat.Pre_plat,
                            IdPlat = plat.Id_plat,
                            IdPed = ped.Id_ped
                        };
            dcart = query.ToList();

            return Json(dcart, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public bool Carrito(ModPedidos mp)
        {
            Boolean guardar = false;

            Pedidos ped = new Pedidos
            {
                IdFacu_ped = mp.IdFac,
                IdUsu_ped = mp.IdUsr,
                IdPlat_ped = mp.IdPlat
            };

            db.Pedidos.Add(ped);
            db.SaveChanges();
            guardar = true;

            return guardar;
        }

        [HttpDelete]
        public void DeleteItem(int id)
        {
            Pedidos deletePedido = (from Ped in db.Pedidos
                                   where Ped.Id_ped == id
                                   select Ped).FirstOrDefault();

            db.Pedidos.Remove(deletePedido);
            db.SaveChanges();
        }
    }
}