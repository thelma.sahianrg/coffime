﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CofFIME.Models.Core;
using CofFIME.Models.Service;

namespace CofFIME.Controllers.Facultades
{
    public class FODController : Controller
    {
        private PlatillosService platService = new PlatillosService();
        // GET: FOD
        public ActionResult Index()
        {
            List<ModPlatillos> listaPlatillos = platService.ObtenerPlatillos(10);
            List<ModPlatillos> platDest = platService.ObtenerPlatillosDest(10);
            ViewBag.destacados = platDest;
            ViewBag.platillos = listaPlatillos;
            return View();
        }
    }
}