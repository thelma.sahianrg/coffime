﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CofFIME.Models.Core;
using CofFIME.Models.Service;

namespace CofFIME.Controllers.Facultades
{
    public class FCFMController : Controller
    {
        private PlatillosService platService = new PlatillosService();
        // GET: FCFM
        public ActionResult Index()
        {
            List<ModPlatillos> listaPlatillos = platService.ObtenerPlatillos(6);
            List<ModPlatillos> platDest = platService.ObtenerPlatillosDest(6);
            ViewBag.destacados = platDest;
            ViewBag.platillos = listaPlatillos;
            return View();
        }
    }
}