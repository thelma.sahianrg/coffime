﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CofFIME.Models.Core;
using CofFIME.Models.Service;

namespace CofFIME.Controllers.Facultades
{
    public class FIMEController : Controller
    {
        private PlatillosService platService = new PlatillosService();
        // GET: FIME
        public ActionResult Index()
        {
            List<ModPlatillos> listaPlatillos = platService.ObtenerPlatillos(1);
            List<ModPlatillos> platDest = platService.ObtenerPlatillosDest(1);

            ViewBag.platillos = listaPlatillos;
            ViewBag.destacados = platDest;
            return View();
        }
    }
}