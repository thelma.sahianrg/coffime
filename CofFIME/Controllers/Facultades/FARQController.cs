﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CofFIME.Models.Core;
using CofFIME.Models.Service;

namespace CofFIME.Controllers.Facultades
{
    public class FARQController : Controller
    {
        private PlatillosService platService = new PlatillosService();
        // GET: FARQ
        public ActionResult Index()
        {
            List<ModPlatillos> listaPlatillos = platService.ObtenerPlatillos(4);
            List<ModPlatillos> platDest = platService.ObtenerPlatillosDest(4);
            ViewBag.destacados = platDest;
            ViewBag.platillos = listaPlatillos;
            return View();
        }
    }
}