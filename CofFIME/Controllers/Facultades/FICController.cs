﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CofFIME.Models.Core;
using CofFIME.Models.Service;

namespace CofFIME.Controllers.Facultades
{
    public class FICController : Controller
    {
        private PlatillosService platService = new PlatillosService();
        // GET: FIC
        public ActionResult Index()
        {
            List<ModPlatillos> listaPlatillos = platService.ObtenerPlatillos(9);
            List<ModPlatillos> platDest = platService.ObtenerPlatillosDest(9);
            ViewBag.destacados = platDest;
            ViewBag.platillos = listaPlatillos;
            return View();
        }
    }
}