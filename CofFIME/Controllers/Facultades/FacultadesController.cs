﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CofFIME.Models.Core;
using CofFIME.Models.Service;

namespace CofFIME.Controllers.Facultades
{
    public class FacultadesController : Controller
    {
        private FacultadesService facuService = new FacultadesService();

        // GET: Facultades
        public ActionResult Index()
        {
            List<ModFacultades> listafacultades = facuService.ObtenerFacultades();
            ViewBag.Facultades = listafacultades;

            return View();
        }
    }
}