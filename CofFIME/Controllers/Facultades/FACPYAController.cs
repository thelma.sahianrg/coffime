﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using CofFIME.Models.Core;
using CofFIME.Models.Service;

namespace CofFIME.Controllers
{
    public class FACPYAController : Controller
    {
        private PlatillosService platService = new PlatillosService();
        // GET: FACPYA
        public ActionResult Index()
        {
            List<ModPlatillos> listaPlatillos = platService.ObtenerPlatillos(3);
            List<ModPlatillos> platDest = platService.ObtenerPlatillosDest(3);
            ViewBag.destacados = platDest;
            ViewBag.platillos = listaPlatillos;
            return View();
        }
    }
}