﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CofFIME.Models.Core;
using CofFIME.Models.Service;

namespace CofFIME.Controllers.Facultades
{
    public class FCQController : Controller
    {
        private PlatillosService platService = new PlatillosService();
        // GET: FCQ
        public ActionResult Index()
        {
            List<ModPlatillos> listaPlatillos = platService.ObtenerPlatillos(7);
            List<ModPlatillos> platDest = platService.ObtenerPlatillosDest(7);
            ViewBag.destacados = platDest;
            ViewBag.platillos = listaPlatillos;
            return View();
        }
    }
}