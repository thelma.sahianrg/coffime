﻿using CofFIME.Models;
using CofFIME.Models.Core;
using CofFIME.Models.Data;
using CofFIME.Models.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CofFIME.Controllers
{
    public class RegisterController : Controller
    {
        private FacultadesService _facultadesService = new FacultadesService();
        private UsuariosService _usuariosService = new UsuariosService();

        public RegisterController()
        {
        }

        // GET: Register
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Register(RegisterViewModel model) {
            try
            {
                ModFacultades objFacultad = _facultadesService.FindFacultadByNombreFacultad(model.CurrentFacultad);
                string objEmail = _usuariosService.FindEmail(model.Email);
                if (ModelState.IsValid && objEmail == null) {
                    Usuarios user = new Usuarios
                    {
                        Contr_usu = model.Password,
                        Nom_usu = model.FirstName,
                        Email_usu = model.Email,
                        IdFacu_usu = objFacultad.Id_facu
                    };

                    _usuariosService.CreateUser(user);
                    Session["FirstName"] = user.Nom_usu;
                    Session["Email"] = user.Email_usu;
                    return RedirectToAction("index", "home");
                }
                else
                {
                    ViewBag.error = "El email " + model.Email + " ya está en uso. Favor de validar";
                }

            }catch(Exception e)
            {
                throw new System.ArgumentException(e.ToString());
            }
            return View(model);
        }
    }
}