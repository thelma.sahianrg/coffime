﻿using System.Web;
using System.Web.Optimization;

namespace CofFIME
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/template").Include(
                        "~/Scripts/Template/jquery.js",
                        "~/Scripts/Template/bootstrap.min.js",
                        "~/Scripts/Template/jquery.parallax.js",
                        "~/Scripts/Template/smoothscroll.js",
                        "~/Scripts/Template/nivo-lightbox.min.js",
                        "~/Scripts/Template/wow.min.js",
                        "~/Scripts/Template/custom.js"));

            bundles.Add(new ScriptBundle("~/bundles/pedidos").Include(
                        "~/Scripts/Template/jquery.js",
                        "~/Scripts/Template/jquery.parallax.js",
                        "~/Scripts/Template/smoothscroll.js",
                        "~/Scripts/Template/nivo-lightbox.min.js",
                        "~/Scripts/Template/wow.min.js",
                        "~/Scripts/Template/custom.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));
        }
    }
}
